/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import clases.Items;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author HOME
 */
@Stateless
public class ItemsFacade extends AbstractFacade<Items> {
    @PersistenceContext(unitName = "gestor_ambientes_360PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ItemsFacade() {
        super(Items.class);
    }
    
    public List<Items> findByInventarioId(int inventario_id){
        String sentencia = "SELECT * FROM Items i WHERE i.item_inventario_id = '" + inventario_id +"'";
        System.out.println(sentencia);
        Query q = em.createNativeQuery(sentencia, Items.class);
        return q.getResultList();
    }
}
