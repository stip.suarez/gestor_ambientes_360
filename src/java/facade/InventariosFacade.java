/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import clases.Inventarios;
import clases.Items;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author HOME
 */
@Stateless
public class InventariosFacade extends AbstractFacade<Inventarios> {
    @PersistenceContext(unitName = "gestor_ambientes_360PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public InventariosFacade() {
        super(Inventarios.class);
    }
    //mi codigo
    public List<Items> findByInventarioId(Inventarios inventario){
        int inventario_id=inventario.getInventarioId();
        String sentencia = "SELECT * FROM Items i WHERE i.item_inventario_id = '" + inventario_id +"'";
//        System.out.println(sentencia);
        Query q = em.createNativeQuery(sentencia, Items.class);
//        System.out.println("Lista: "+q.getResultList());
        return q.getResultList();
    }
}
