/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import clases.Ciudad;
import clases.Departamentos;
import clases.Usuarios;
import java.util.List;
import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author HOME
 */
@Stateless
public class CiudadFacade extends AbstractFacade<Ciudad> {
    @PersistenceContext(unitName = "gestor_ambientes_360PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CiudadFacade() {
        super(Ciudad.class);
    }
  
//Mi codigo
    private List<Ciudad> lista_ciudades_dpto;

    public List<Ciudad> getLista_ciudades_dpto() {
        return lista_ciudades_dpto;
    }

    public void setLista_ciudades_dpto(List<Ciudad> lista_ciudades_dpto) {
        this.lista_ciudades_dpto = lista_ciudades_dpto;
    }
    
    public List<Ciudad>  asignarCiudades(Departamentos dpto){
      
         try {
            String sentencia = "SELECT * FROM Ciudad c WHERE c.ciudad_departamento_id = '"+dpto.getDepartamentoId()+"'";
            Query q = em.createNativeQuery(sentencia, Ciudad.class);
              lista_ciudades_dpto=q.getResultList();
              System.out.println("fin asignar ciudades:"+lista_ciudades_dpto);
            return q.getResultList();

        } catch (Exception e) {
            FacesContext context = FacesContext.getCurrentInstance();
             System.out.println("Error asignando ciudades: "+e.getLocalizedMessage());
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error al realizar la consulta en la BD: asignando ciudades " + e + "\nLocalize:" + e.getLocalizedMessage(), "   " + e.getLocalizedMessage()));
        }
        return null;
    }
    
}
