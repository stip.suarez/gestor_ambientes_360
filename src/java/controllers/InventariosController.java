package controllers;

import clases.Ambientes;
import clases.Inventarios;
import controllers.util.JsfUtil;
import controllers.util.PaginationHelper;
import facade.InventariosFacade;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;

@Named("inventariosController")
@SessionScoped
public class InventariosController implements Serializable {

    private Inventarios current;
    private DataModel items = null;
    @EJB
    private facade.InventariosFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    public InventariosController() {
    }

    public Inventarios getSelected() {
        if (current == null) {
            current = new Inventarios();
            selectedItemIndex = -1;
        }
        return current;
    }

    private InventariosFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (Inventarios) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View.xhtml";
    }

    public String prepareCreate() {
        current = new Inventarios();
        selectedItemIndex = -1;
        return "administracion.xhtml";
    }

    public String create() {
        try {
            System.out.println("Creando inventario: " + current);
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("InventariosCreated"));
            System.out.println("Inventario Creodo con exito!");
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            System.out.println("Error de persistencia al crear inventario: " + e);
            return null;
        }
    }

    public void prepareEdit() {
        current = (Inventarios) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
    }

    public String update() {
        try {
            System.out.println("Inventario a actualizar: "+current);
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("InventariosUpdated"));
            return "administracion.xhtml";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (Inventarios) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "administracion.xhtml";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("InventariosDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public Inventarios getInventarios(java.lang.Integer id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = Inventarios.class)
    public static class InventariosControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            InventariosController controller = (InventariosController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "inventariosController");
            return controller.getInventarios(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Inventarios) {
                Inventarios o = (Inventarios) object;
                return getStringKey(o.getInventarioId());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Inventarios.class.getName());
            }
        }

    }
//Mi codigo crear lista con todos los Inventarios
    private List<Inventarios> inventarios;
    private Inventarios inventario_select;

    public Inventarios getInventario_select() {
        return inventario_select;
    }

    public void setInventario_select(Ambientes ambiente) {
        try {

            this.inventario_select = ambiente.getInventarioInventarioId();
//            getSelected();
//            this.current = getSelected();
//            System.out.println("selec... " + getSelected());
//            System.out.println("get ambiente selecttt... " + getAmbienteSelec());
//            //current=inventario_select;
//            System.out.println("Ambiente select: " + ambiente);

        } catch (Exception e) {
            System.out.println("Error: " + e);
        }
    }

    public List<Inventarios> getInventarios() {
        return inventarios = ejbFacade.findAll();
    }

    public void setInventarios(List<Inventarios> inventarios) {
        this.inventarios = inventarios;
    }
    private Ambientes ambienteSelec;

    public Ambientes getAmbienteSelec() {
        
        return ambienteSelec;
    }

    public void setAmbienteSelec(Ambientes ambienteSelec) {
         
        this.ambienteSelec = ambienteSelec;
    }

    private boolean headerButtonsDisabled = true;
//add a List object for all books (bookList) with getter and setter

    public boolean isHeaderButtonsDisabled() {
        return headerButtonsDisabled;
    }

    public void setHeaderButtonsDisabled(boolean headerButtonsDisabled) {
        this.headerButtonsDisabled = headerButtonsDisabled;
    }

    public void onRowSelectDataTable() {
        this.setHeaderButtonsDisabled(false);
        
    }
     public void onRowSelect(SelectEvent event) {
         ambienteSelec=(Ambientes) event.getObject();
         inventario_select=ambienteSelec.getInventarioInventarioId();
         current=getInventario_select();
         //Buscar la lista de items según el id del inventario seleccionado
        current.setItemsCollection(ejbFacade.findByInventarioId(inventario_select));
//        FacesMessage msg = new FacesMessage("Inventario Selected: "+getInventario_select(), ""+getInventario_select());
//        FacesContext.getCurrentInstance().addMessage(null, msg);
         System.out.println("inventario Current: "+current);
    }
 
    public void onRowUnselect(UnselectEvent event) {
        setInventario_select((Ambientes) event.getObject());
        FacesMessage msg = new FacesMessage("Ambiente Unselected", ""+getInventario_select());
        FacesContext.getCurrentInstance().addMessage(null, msg);
        System.out.println(""+msg);
    }
}
