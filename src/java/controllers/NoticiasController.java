package controllers;

import clases.Noticias;
import clases.Usuarios;
import static controllers.UsuariosController.getCurrent;
import controllers.util.JsfUtil;
import controllers.util.PaginationHelper;
import facade.NoticiasFacade;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.Event;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.UploadedFile;

@Named("noticiasController")
@SessionScoped
public class NoticiasController implements Serializable {

    private Noticias current;
    private DataModel items = null;
    @EJB
    private facade.NoticiasFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    public NoticiasController() {
    }

    public Noticias getSelected() {
        if (current == null) {
            current = new Noticias();
            current.setNoticiasPK(new clases.NoticiasPK());
            selectedItemIndex = -1;
        }
        return current;
    }

    private NoticiasFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (Noticias) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new Noticias();
        current.setNoticiasPK(new clases.NoticiasPK());
        selectedItemIndex = -1;
        return "perfiles.xhtml";
    }

    public String create() {
        try {//obtener el usuario actual que ha iniciado sesion
            System.out.println("Vamos a crear la noticia");
            FacesContext context2 = FacesContext.getCurrentInstance();
            HttpSession sessionv = (HttpSession) context2.getExternalContext().getSession(true);
            usrActual = (Usuarios) sessionv.getAttribute("user");
            System.out.println("usr " + usrActual.getUsrId());

            current.getNoticiasPK().setNoticiaUsrId(usrActual.getUsrId());
            System.out.println("Noticia current: " + current.getNoticiasPK());
            getFacade().create(current);
            System.out.println("2");
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("NoticiasCreated"));
            System.out.println("3");
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            System.out.println("Error al crear la noticia: " + e.toString());
            return null;
        }
    }

    public void prepareEdit() {
        current = (Noticias) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
    }

    public String update() {
        System.out.println("Vamos a actualizar la noticia " + current.toString());
        try {
            //current=(Noticias) event.getObject();
            //current.getNoticiasPK().setNoticiaUsrId(current.getUsuarios().getUsrId());
            System.out.println("Current noticia: " + getSelected());
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("NoticiasUpdated"));
            System.out.println("Actualizado con exito");
            return "administracion.xhtml";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            System.out.println("Error al actualizar: " + e.toString());
            return null;
        }
    }

    public String destroy() {
        current = (Noticias) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "administracion.xhtml";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("NoticiasDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public Noticias getNoticias(clases.NoticiasPK id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = Noticias.class)
    public static class NoticiasControllerConverter implements Converter {

        private static final String SEPARATOR = "#";
        private static final String SEPARATOR_ESCAPED = "\\#";

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            NoticiasController controller = (NoticiasController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "noticiasController");
            return controller.getNoticias(getKey(value));
        }

        clases.NoticiasPK getKey(String value) {
            clases.NoticiasPK key;
            String values[] = value.split(SEPARATOR_ESCAPED);
            key = new clases.NoticiasPK();
            key.setNoticiaId(Integer.parseInt(values[0]));
            key.setNoticiaUsrId(Integer.parseInt(values[1]));
            return key;
        }

        String getStringKey(clases.NoticiasPK value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value.getNoticiaId());
            sb.append(SEPARATOR);
            sb.append(value.getNoticiaUsrId());
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Noticias) {
                Noticias o = (Noticias) object;
                return getStringKey(o.getNoticiasPK());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Noticias.class.getName());
            }
        }

    }

//Mi código
    Usuarios usrActual;

    public Usuarios getUsrActual() {
        return usrActual;
    }

    public void setUsrActual(Usuarios usrActual) {
        this.usrActual = usrActual;
    }

    //archivo
    private Part minuta;
    //para la tabla
    private List<Noticias> noticias; //Mostrar las noticias en la tabla
    private Noticias noticia_selec;

    public Part getMinuta() {

        return minuta;
    }

    public void setMinuta(Part minuta) {
        this.minuta = minuta;
    }

    public List<Noticias> getNoticias() {
        return noticias = ejbFacade.findAll();
    }

    public void setNoticias(List<Noticias> noticias) {
        this.noticias = noticias;
    }

    public Noticias getNoticia_selec() {
        return noticia_selec = getSelected();
    }

    public void setNoticia_selec(Noticias noticia_selec) {
        this.noticia_selec = noticia_selec;
    }

    public void leer_mas() throws IOException {
        JsfUtil.addSuccessMessage("Vamos a leer la noticia: " + noticia_selec);
        current = noticia_selec;
        FacesContext.getCurrentInstance().getExternalContext().redirect("/gestor_ambientes_360/faces/web/noticias/verNoticia.xhtml");
    }
    //subir archivo noticia
    //Cambiar foto
    //Subir Archivos
    private UploadedFile file;
    private InputStream in;

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public UploadedFile getFile() {
        return file;
    }

    public InputStream getIn() {
        return in;
    }

    public void setIn(InputStream in) {
        this.in = in;
    }

    public void handleFileUploadNoticia(FileUploadEvent event) { //Cambiar foto

        try {

            System.out.println("Subiendo Noticia.... ");
            setIn(event.getFile().getInputstream());
            setFile(event.getFile());
            //String nombre_file = getCurrent().getUsrId() + "." + getExtencion(getFile().getFileName());
            //buscan la antidad de noticas para asignar el id
            int id_noticia;
            if (current == null) {
                id_noticia = ejbFacade.findAll().size() + 1;
            }
            else id_noticia=current.getNoticiasPK().getNoticiaId();
            String nombre_file = (id_noticia + "." + getExtencion(getFile().getFileName()));
            System.out.println("nombre file: " + nombre_file);
            System.out.println("noti id: " + getSelected().getNoticiasPK());
            TransferFile(nombre_file, "");
            current.setNoticiaImagen("" + id_noticia + "." + getExtencion(file.getFileName()));
            current.getNoticiasPK().setNoticiaId(id_noticia);
            System.out.println("Subiendo foto.... exito y actualizado usr actual: " + nombre_file);
            //FacesContext.getCurrentInstance().getExternalContext().redirect("/gestor_ambientes_360/faces/web/usuarios/perfiles.xhtml");

        } catch (Exception e) {
            System.out.println("Error al subir la foto...  " + e);
            JsfUtil.addErrorMessage("Error al subir la foto: " + e);
        }
    }

    public String TransferFile(String nombreArchivo, String directorioTipo) {
        try {
            String direccion = rutaNoticia() + directorioTipo;
            //Crear carpeta de usuarios
            System.out.println("direccion: " + direccion);
            File folder = new File(direccion);
            if (!folder.exists()) {
                folder.mkdirs(); // esto crea la carpeta java, independientemente que exista el path completo, si no existe crea toda la ruta necesaria                 
            }
            OutputStream out = new FileOutputStream(new File(direccion + "/" + nombreArchivo));
            int read;
            byte[] bytes = new byte[(int) getFile().getSize()];
            while ((read = in.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            in.close();
            out.flush();
            return direccion;
        } catch (IOException e) {
            JsfUtil.addErrorMessage("Error al subir el archivo: " + e.toString());
            return null;
        }
    }

    public String rutaNoticia() throws UnsupportedEncodingException { //Retorna la direccion de un archivo para ubicar una ruta
        //String rutaca = ArchivosController.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        String rutaca = UsuariosController.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        rutaca = URLDecoder.decode(rutaca, "utf-8");
        String[] base = rutaca.split("/");
        String direccion = "";
        for (int i = 0; i < base.length - 6; i++) {
            direccion = direccion + base[i] + "/";
        }

        return direccion + "web/" + "Recursos/ImagenesNoticias";
    }

    public String getExtencion(String FileName) {
        String extValidate;
        String ext = FileName;
        if (ext != null) {
            extValidate = ext.substring(ext.indexOf(".") + 1);
        } else {
            extValidate = null;
        }
        return extValidate;
    }

    public void onDateSelect(SelectEvent event) {
//        FacesContext facesContext = FacesContext.getCurrentInstance();
//        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
//        facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Date Selected", format.format(event.getObject())));
        current.setNoticiaFecha((Date) event.getObject());
        //System.out.println("Current date select.. "+current);
    }
}
