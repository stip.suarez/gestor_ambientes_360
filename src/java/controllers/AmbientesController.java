package controllers;

import clases.Ambientes;
import controllers.util.JsfUtil;
import controllers.util.PaginationHelper;
import facade.AmbientesFacade;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

@Named("ambientesController")
@SessionScoped
public class AmbientesController implements Serializable {

    private Ambientes current;
    private DataModel items = null;
    @EJB
    private facade.AmbientesFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    public AmbientesController() {
    }

    public Ambientes getSelected() {
        if (current == null) {
            current = new Ambientes();
            selectedItemIndex = -1;
        }
        return current;
    }
    public void setInventarioSelect(Ambientes ambiente) {
        current = ambiente;
        ambiente.getInventarioInventarioId();
    }

    private AmbientesFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (Ambientes) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new Ambientes();
        selectedItemIndex = -1;
        return "administracion.xhtml";
    }

    public String create() {
        try {
            getFacade().create(current);
            //actualizar lista de ambientes
            items.setWrappedData(ejbFacade.findAll());
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("AmbientesCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (Ambientes) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("AmbientesUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (Ambientes) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("AmbientesDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public Ambientes getAmbientes(java.lang.Integer id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = Ambientes.class)
    public static class AmbientesControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            AmbientesController controller = (AmbientesController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "ambientesController");
            return controller.getAmbientes(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Ambientes) {
                Ambientes o = (Ambientes) object;
                return getStringKey(o.getAmbienteId());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Ambientes.class.getName());
            }
        }

    }

     //subir archivo noticia
    //Cambiar foto
    //Subir Archivos
    private UploadedFile file;
    private InputStream in;

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public UploadedFile getFile() {
        return file;
    }

    public InputStream getIn() {
        return in;
    }

    public void setIn(InputStream in) {
        this.in = in;
    }

    public void handleFileUploadAmbiente(FileUploadEvent event) { //Cambiar foto

        try {

            System.out.println("Subiendo Noticia.... ");
            setIn(event.getFile().getInputstream());
            setFile(event.getFile());
            //String nombre_file = getCurrent().getUsrId() + "." + getExtencion(getFile().getFileName());
            //buscan la antidad de noticas para asignar el id
            int id_ambiente= ejbFacade.findAll().size()+1;
            String nombre_file = (id_ambiente+ "." + getExtencion(getFile().getFileName()));
            System.out.println("nombre file: "+nombre_file);
            TransferFile(nombre_file, "");
            current.setAmbienteImg360(""+id_ambiente+"."+getExtencion(file.getFileName()));
            System.out.println("Subiendo foto.... exito : "+nombre_file);
            //FacesContext.getCurrentInstance().getExternalContext().redirect("/gestor_ambientes_360/faces/web/usuarios/perfiles.xhtml");

        } catch (Exception e) {
            System.out.println("Error al subir la foto...  " + e);
            JsfUtil.addErrorMessage("Error al subir la foto: " + e);
        }
    }
     public String TransferFile(String nombreArchivo, String directorioTipo) {
        try {
            String direccion = rutaAmbientes()+ directorioTipo;
            //Crear carpeta de usuarios
            System.out.println("direccion: "+direccion);
            File folder = new File(direccion);
            if (!folder.exists()) {
                folder.mkdirs(); // esto crea la carpeta java, independientemente que exista el path completo, si no existe crea toda la ruta necesaria                 
            }
            OutputStream out = new FileOutputStream(new File(direccion + "/" + nombreArchivo));
            int read;
            byte[] bytes = new byte[(int) getFile().getSize()];
            while ((read = in.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            in.close();
            out.flush();
            return direccion;
        } catch (IOException e) {
            JsfUtil.addErrorMessage("Error al subir el archivo: " + e.toString());
            return null;
        }
    }

    public String rutaAmbientes() throws UnsupportedEncodingException { //Retorna la direccion de un archivo para ubicar una ruta
        //String rutaca = ArchivosController.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        String rutaca = UsuariosController.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        rutaca = URLDecoder.decode(rutaca, "utf-8");
        String[] base = rutaca.split("/");
        String direccion = "";
        for (int i = 0; i < base.length - 6; i++) {
            direccion = direccion + base[i] + "/";
        }

        return direccion + "web/" + "Recursos/ImagenesAmbientes";
    }

    public String getExtencion(String FileName) {
        String extValidate;
        String ext = FileName;
        if (ext != null) {
            extValidate = ext.substring(ext.indexOf(".") + 1);
        } else {
            extValidate = null;
        }
        return extValidate;
    }
}
