package controllers;

import static bean.InterfaceBean.session;
import clases.Ciudad;
import clases.Departamentos;
import clases.Solicitudes;
import clases.Usuarios;
import controllers.util.JsfUtil;
import controllers.util.PaginationHelper;
import controllers.util.PasswordService;
import facade.SolicitudesFacade;
import facade.UsuariosFacade;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.faces.validator.ValidatorException;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.FlowEvent;
import org.primefaces.model.UploadedFile;

@Named("usuariosController")
//@javax.faces.bean.SessionScoped
@SessionScoped
public class UsuariosController implements Serializable {

    private static Usuarios current;
    private DataModel items = null;
    @EJB
    private facade.UsuariosFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;

    public UsuariosController() {
        this.actualPass = "";
    }

    public Usuarios getSelected() {
        if (current == null) {
            current = new Usuarios();
            selectedItemIndex = -1;
        }
        return current;
    }

    public static Usuarios getCurrent() {
        return current;
    }

    public static void setCurrent(Usuarios current) {
        UsuariosController.current = current;
    }

    private UsuariosFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (Usuarios) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

//Editamos el metodo create en la ultima seccion y prepare create
//    public String create() {
//        try {
//            getFacade().create(current);
//            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("UsuariosCreated"));
//            return prepareCreate();
//        } catch (Exception e) {
//            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
//            return null;
//        }
//    }
    public void prepareEdit() {
        current = (Usuarios) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
    }

    public String update() {
        try {
            System.out.println("Vamos a actualizar 1 ");
            System.out.println("Vamos a actualizar usr: " + current);
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("UsuariosUpdated"));
            System.out.println("Usuario actualizado");
            return "administracion.xhtml";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            System.out.println("Error al actualizar el Usuario");
            return null;
        }
    }

    public String destroy() {
        current = (Usuarios) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "administracion.xhtml";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("UsuariosDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public Usuarios getUsuarios(java.lang.Integer id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = Usuarios.class)
    public static class UsuariosControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            UsuariosController controller = (UsuariosController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "usuariosController");
            return controller.getUsuarios(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Usuarios) {
                Usuarios o = (Usuarios) object;
                return getStringKey(o.getUsrId());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Usuarios.class.getName());
            }
        }

    }
//CODIGO PERSONAL

    private Usuarios usuarioSelect;
    private static Usuarios usuarioActual; //Permita pasar al Usuario a travez de muchos controlers
    private Usuarios usuarioActualVista; //Permite mostrar los datos del usuario actual en la vista
    private boolean skip;//varible de session
    private String usrPass2;
    @EJB
    private facade.SolicitudesFacade ejbSolicitudFacade;

    private SolicitudesFacade getSolicitudFacade() {
        return this.ejbSolicitudFacade;
    }
    //este es mi codigo

    public String getUsrPass2() {
        return usrPass2;
    }

    public void setUsrPass2(String usrPass2) {
        this.usrPass2 = usrPass2;
    }

    public Usuarios getUsuarioSelect() {
        return usuarioSelect;
    }

    public void setUsuarioSelect(Usuarios usuarioSelect) {
        this.usuarioSelect = usuarioSelect;
    }

    public void setUsuarioActualVista(Usuarios usuarioActualVista) {
        this.usuarioActualVista = usuarioActualVista;
    }

    public Usuarios getUsuarioActualVista() {
        return usuarioActualVista;
    }

    public Usuarios getUsuarioActual() {
        return usuarioActual;
    }

    public static void setUsuarioActual(Usuarios usuarioActual) {
        UsuariosController.usuarioActual = usuarioActual;
    }

    public String prepareCreate() {
        current = new Usuarios();
        selectedItemIndex = -1;
        setUsrPass2(null);
        return "index.xhtml";
    }

    public void create() {
        try {
            System.out.println("VAmos a crear usr: " + current.toString());
            //Encriptar contraseña

            getFacade().create(current);
            current.setUsrPass(encriptarPass(current.getUsrPass()));
            current.setUsrFoto("default.png");
            current.setUsrEstado(false);
            getFacade().edit(current);
            enviarSolicitud();
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("UsuariosCreated"));
//            FacesContext context2 = FacesContext.getCurrentInstance();
//            HttpSession sessionv = (HttpSession) context2.getExternalContext().getSession(true);
//            sessionv.setAttribute("user", current);
//            session.setActiva(true);
            System.out.println("Creando con exito, prepare crate" + current.toString());
            //prepareCreate();
            JsfUtil.addSuccessMessage("Esperarando a que acepten tu solicitud.");
            prepareCreate();
            setUsrPass2(null);
            System.out.println("Usuario Creado con exito: Espere respuesta a su solicitud" + current.toString());
            //FacesContext.getCurrentInstance().getExternalContext().redirect("/gestor_ambientes_360/");

        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            System.out.println("Error al crear el usr " + e);

        }
    }

    public void enviarSolicitud() {
        try {

            System.out.println("Enviando solicitud ... ");
            int cc_usr_solicitud = Integer.parseInt(current.getUsrNumDoc());
            Date fechaActual = new Date();
            String estado = "0";
            Solicitudes solicitud;
            solicitud = new Solicitudes();
            solicitud.setSolicitudCc(cc_usr_solicitud);
            solicitud.setSolicitudFecha(fechaActual);
            solicitud.setSolicitudEstado(estado);
            solicitud.setSolicitudUsrId(current);
            Integer solicitud_id = ejbSolicitudFacade.findAll().size() + 1;
            solicitud.setSolicitudId(solicitud_id);
            System.out.println("Solicitud Id... " + solicitud.toString());
            ejbSolicitudFacade.create(solicitud);
            System.out.println("solicitud enviada!!!!!");

        } catch (Exception e) {
            System.out.println("Error al enviar la solicitud: " + e.toString());
        }
    }
//
//    public void create() {
//        try {
//          
//            JsfUtil.addSuccessMessage("Creando: pass ..." );
//            JsfUtil.addSuccessMessage("Creando: pass current" + current.toString());
//            JsfUtil.addSuccessMessage("Creando: pass selected" + getSelected().toString());
//            System.out.println("a:a asd" );
//            System.out.println("a: " + usuarioSelect.getUsrPass());
//            current.setUsrPass(encriptarPass(current.getUsrPass()));
//            current.setUsrEstado(true);
//            current.setUsrId(selectedItemIndex);
//            getFacade().create(current);
//            setUsuarioActual(current);
//            FacesContext context2 = FacesContext.getCurrentInstance();
//            HttpSession sessionv = (HttpSession) context2.getExternalContext().getSession(true);
//            sessionv.setAttribute("user", current);
//            session.setActiva(true);
//            JsfUtil.addSuccessMessage("Sesion Iniciada");
//            prepareCreate();
//            setUsrPass2(null);
//            FacesContext.getCurrentInstance().getExternalContext().redirect("/gestor_ambientes_360/");
//        } catch (Exception e) {
//            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
//            JsfUtil.addErrorMessage("Error detail: "+e.toString());
//            System.out.println("Error detail: create"+e.toString());
//        }
//    }
    //Variables de registro 

    public boolean isSkip() {
        return skip;
    }

    public void setSkip(boolean skip) {
        this.skip = skip;
    }

    public String onFlowProcess(FlowEvent event) {
        if (current.getUsrPass().compareTo(usrPass2) != 0) {
            JsfUtil.addErrorMessage("Erros de contraseña");
            return event.getOldStep();
        }
        if (skip) {
            skip = false;   //reset in case user goes back
            return "confirmacion";
        } else {
            return event.getNewStep();
        }
    }

    public String encriptarPass(String palabra) { //Encripta la contraseñas
        PasswordService pws = PasswordService.getInstance();
        String hash;
        System.out.println("Palabra a encriptar:" + palabra);
        try {
            hash = pws.encrypt(palabra);
            System.out.println("hash: " + hash);
            return hash;
        } catch (Exception e) {
            System.out.println("Error de encriptacion : " + e.toString());
            JsfUtil.addErrorMessage("Error de encriptación: " + e.toString());
            return null;
        }
    }
    //Ver perifil de usuarios

    public String sexo() {
        if (getUsuarioSelect().getUsrSexo()) {
            return "Hombre";
        } else {
            return "Mujer";
        }
    }

    public String sexoUsuarioVista() {
        if (getUsuarioActual().getUsrSexo()) {
            return "Hombre";
        } else {
            return "Mujer";
        }
    }

    public void setPerfilPersonal() {
        setUsuarioSelect(getUsuarioActual());
//        ColectivosController.setUsuario(getUsuarioSelect());
    }

    public void setPerilUsuario(Usuarios usuario) throws IOException { //Asigna el usuario para ver el perfil
        FacesContext.getCurrentInstance().getExternalContext().redirect("/gestor_ambientes_360/faces/web/usuarios/indexPerfiles.xhtml");
    }
    //
    private DataModel itemsAdminSelect = null;

    //Cambiar clave
    String actualPass;
    private String nuevaPass;
    String nuevaPassConfir;

    public String getActualPass() {
        return actualPass;
    }

    public void setActualPass(String actualPass) {
        this.actualPass = actualPass;
    }

    public String getNuevaPass() {
        return nuevaPass;
    }

    public void setNuevaPass(String nuevaPass) {
        this.nuevaPass = nuevaPass;
    }

    public String getNuevaPassConfir() {
        return nuevaPassConfir;
    }

    public void setNuevaPassConfir(String nuevaPassConfir) {
        this.nuevaPassConfir = nuevaPassConfir;
    }

    public void validarPassActual(FacesContext arg0, UIComponent arg1, Object arg2) throws ValidatorException {
        //obtener el usr actual
//        FacesContext context2 = FacesContext.getCurrentInstance();
//        HttpSession sessionv = (HttpSession) context2.getExternalContext().getSession(true);
//        current=(Usuarios) sessionv.getAttribute("user");
        System.out.println("Arg 2 validarPassActual: " + arg2.toString());
        String pas = encriptarPass(arg2.toString());
//        String actual = current.getUsrPass();
        String actual = usuarioActual.getUsrPass();
        System.out.println("actual: " + actual);
        //actual=encriptarPass(actual);
        //setActualPass(actual);
        //this.actualPass=actual;
        System.out.println("contraseña act: " + actual);
        System.out.println("contraseña new pass: " + pas);
        if (!pas.equals(actual)) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "La contraseña no es la actual ", "La contraseña no es la actual: "));
        }
    }

    public void validarNueva(FacesContext arg0, UIComponent arg1, Object arg2) {
        setNuevaPass(arg2.toString());
    }

    public void validarNuevaConfirmar(FacesContext arg0, UIComponent arg1, Object arg2) throws ValidatorException {
        nuevaPassConfir = arg2.toString();
        if (!getNuevaPassConfir().equals(getNuevaPass())) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Las contraseñas no coinciden", "Las contraseñas no coinciden "));
        }
    }

    public void actualizarContraseña() {
        try {
            //Encriptamos primero la contraseña
            current.setUsrPass(encriptarPass(this.nuevaPass));
            update();
            FacesContext context = FacesContext.getCurrentInstance();
            JsfUtil.addSuccessMessage("Contraseña actualizada con éxito!");
            context.getCurrentInstance().getExternalContext().redirect("/gestor_ambientes_360/faces/web/usuarios/perfiles.xhtml");
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Error al actualizar la contraseña: " + e);
            System.out.println("Error al actualizar la contraseña");
        }
    }

    //Inciar Sesion
    private String cedula;
    private String contrasena;

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public boolean isActiva() {
        return session.isActiva();

    }

    public void logIn() { //Inicia sesion
        //Para los mensajes de error las dos lineas siguientes
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            Usuarios user = null;
            cedula = getSelected().getUsrNumDoc();
            contrasena = getSelected().getUsrPass();
            //Encriptamos el pass
            contrasena = encriptarPass(contrasena);
            boolean registrado = getFacade().validarLogueo(cedula, contrasena);
            System.out.println("REGISTRADO= " + registrado);
            if (registrado) {
                user = (Usuarios) getFacade().usuarioLogueado(cedula);

                System.out.println("uSR= " + user);
                if (user.getUsrEstado()) {
                    setUsuarioActual(user);
                    setUsuarioActualVista(user);
                    HttpSession sessionv = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                    sessionv.setAttribute("user", user);
                    session.setActiva(true);
                    JsfUtil.addSuccessMessage("Sesion Iniciada");
                    System.out.println("Sesion Iniciada: Bienvenido User: " + usuarioActual.toString());
                    //FacesContext.getCurrentInstance().getExternalContext().redirect("/gestor_ambientes_360/");
                    FacesContext.getCurrentInstance().getExternalContext().redirect("/gestor_ambientes_360/faces/web/usuarios/index.xhtml");

                } else {
                    JsfUtil.addErrorMessage("Su solicitud aun no ha sido procesada. Espere!!!");
                    System.out.println("Su solicitud aun no ha sido procesada. Espere!:" + user.toString());

                }

            } else {
                JsfUtil.addErrorMessage("No está registrado con esos datos!!!");
                System.out.println("No está registrado!:");

            }

        } catch (Exception e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Verifique cédula y/o contraseña! o espere a que se responda su solicitud" + e.toString(), ""));
            JsfUtil.addErrorMessage("Su solicitud aun no ha sido procesada. Espere!");
            System.out.println("verificar cc o paass :" + e.toString());

        }

    }
    //Cerrar Sesion
    //Cerrar Sesion

    public void logOut() {
        System.out.println("Cerrando cession... 0  ");
        try {
            System.out.println("Cerrando cession... 1  ");
            HttpSession sessionv = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
            sessionv.invalidate();
            FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
            System.out.println("Cerrando cession... 2  ");
            session.setActiva(false);
            //getFacade().actulizarEm(UsuariosController.getUsuarioActual());
            JsfUtil.addErrorMessage("cerrando session 2: ");
            System.out.println("Cerrando cession... 2  ");
            getFacade().actulizarEm(getUsuarioActual());
            usuarioActual = null;
            current = null;
            //UsuariosController.setUsuarioActual(null);
            JsfUtil.addSuccessMessage("Cerrar sesion final");
            System.out.println("Cerrando cession... final  ");
            FacesContext.getCurrentInstance().getExternalContext().redirect("/gestor_ambientes_360/");

        } catch (Exception e) {
            JsfUtil.addErrorMessage("Error al Cerrar sesion: " + e);
            System.out.println("Error al cerrar sesion: " + e);
        }
    }

    public void rowSelect() {
        JsfUtil.addSuccessMessage("Entra");
    }

//Lista de usuarios ad ministradores
    private List<Usuarios> usr_admin_list;

    public List<Usuarios> getUsr_admin_list() {
        return usr_admin_list;
    }

    public void setUsr_admin_list(List<Usuarios> usr_admin_list) {
        this.usr_admin_list = usr_admin_list;
    }

    public void buscarAdmins() {
        try {
            System.out.println("vamos a buscar admmins...");
            //setUsr_admin_list(ejbFacade.buscarUsuariosAadmin());
            usr_admin_list = ejbFacade.buscarUsuariosAadmin();
            if (usr_admin_list != null) {
                System.out.println("Encontramos  Admins: " + usr_admin_list.toString());
            } else {
                System.out.println("No hay admins:: " + usr_admin_list.toString());
            }

        } catch (Exception e) {
            System.out.println("Error al cargar usuarios admin: " + e.toString());
        }

    }
    //Cambiar foto
    //Subir Archivos
    private UploadedFile file;
    private InputStream in;

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public UploadedFile getFile() {
        return file;
    }

    public InputStream getIn() {
        return in;
    }

    public void setIn(InputStream in) {
        this.in = in;
    }
//subir imagen de perfil
    public void handleFileUploadFoto(FileUploadEvent event) { //Cambiar foto

        try {

            System.out.println("Subiendo foto.... ");
            setIn(event.getFile().getInputstream());
            setFile(event.getFile());
            String nombre_file= getCurrent().getUsrId()+"."+getExtencion(getFile().getFileName());
            TransferFile(nombre_file, "");
            getCurrent().setUsrFoto(nombre_file);
            ejbFacade.actulizarEm(current);
            System.out.println("Subiendo foto.... exito y actualizado usr actual");
            FacesContext.getCurrentInstance().getExternalContext().redirect("/gestor_ambientes_360/faces/web/usuarios/perfiles.xhtml");

        } catch (Exception e) {
            System.out.println("Error al subir la foto...  " + e);
            JsfUtil.addErrorMessage("Error al subir la foto: "+e);
        }
    }


    public String getExtencion(String FileName) {
        String extValidate;
        String ext = FileName;
        if (ext != null) {
            extValidate = ext.substring(ext.indexOf(".") + 1);
        } else {
            extValidate = null;
        }
        return extValidate;
    }

    public String ruta() throws UnsupportedEncodingException { //Retorna la direccion de un archivo para ubicar una ruta
        //String rutaca = ArchivosController.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        String rutaca = UsuariosController.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        rutaca = URLDecoder.decode(rutaca, "utf-8");
        String[] base = rutaca.split("/");
        String direccion = "";
        for (int i = 0; i < base.length - 6; i++) {
            direccion = direccion + base[i] + "/";
        }
        
        return direccion + "web/" + "Recursos/Imagenes/perfil";
    }

    public String getDirectorio() {

        return "Usuarios/";

    }

    public String TransferFile(String nombreArchivo, String directorioTipo) {
        try {
            String direccion = ruta() + directorioTipo;
            //Crear carpeta de usuarios
            System.out.println("direccion: "+direccion);
            File folder = new File(direccion);
            if (!folder.exists()) {
                folder.mkdirs(); // esto crea la carpeta java, independientemente que exista el path completo, si no existe crea toda la ruta necesaria                 
            }
            OutputStream out = new FileOutputStream(new File(direccion + "/" + nombreArchivo));
            int read;
            byte[] bytes = new byte[(int) getFile().getSize()];
            while ((read = in.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            in.close();
            out.flush();
            return direccion;
        } catch (IOException e) {
            JsfUtil.addErrorMessage("Error al subir el archivo: " + e.toString());
            return null;
        }
    }
//Este es mi código
    // Validar contraseña 
    private String pass1 = "";

    List<Usuarios> listaUsuarios = new ArrayList<>();

    public String getPass1() {
        return pass1;
    }

    public void setPass1(String pass1) {
        this.pass1 = pass1;
    }

    public void asignarCon(FacesContext arg0, UIComponent arg1, Object arg2)
            throws ValidatorException {
        this.pass1 = arg2.toString();

    }

    public void validarCon(FacesContext fc, UIComponent component, Object value) throws ValidatorException {
        try {

            String attribute = (String) component.getAttributes().get("password");
            if (!value.equals(attribute)) {
                FacesMessage message = new FacesMessage();
                message.setSummary("La contraseña no coincide");
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ValidatorException(message);
            }

        } catch (Exception e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage("datosFrom", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Las contraseñas no coinciden"));

        }
    }

    public void nuevaPass(FacesContext arg0, UIComponent arg1, Object arg2) {
        nuevaPass = (String) arg2;
        current.setUsrPass(nuevaPass);
    }

    private boolean verFoto;
    private boolean verFotoSelect;

    public boolean isVerFoto() {
        try {
            final ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
            Usuarios u = getUsuarioActual();
            String foto = "" + u.getUsrId();
            foto+= getExtencion(getFile().getFileName());
            String fileFoto = servletContext.getRealPath("") + File.separator + "/Recursos/Imagenes/perfil/" + File.separator + foto;

            File file = new File(fileFoto);
            return verFoto = file.exists();
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Error en foto");
            return false;
        }
    }

    public boolean isVerFotoSelect() {
        try {
            final ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
            String foto = "";
            if (usuarioSelect != null) {
                foto = "" + this.usuarioSelect.getUsrId();
            }
            String fileFoto = servletContext.getRealPath("") + File.separator + "/Recursos/Imagenes/perfil/" + File.separator + foto;
            File file = new File(fileFoto);
            return verFotoSelect = file.exists();
        } catch (Exception e) {
            return false;
        }
    }
}
