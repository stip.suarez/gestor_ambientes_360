package bean;

import clases.Usuarios;
import facade.UsuariosFacade;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

@ManagedBean(name = "fileuploadcontroller")
@RequestScoped
public class FileUploadController {

    @EJB
    private facade.UsuariosFacade usrEjbFacade;

    public UsuariosFacade getUsrEjbFacade() {
        return usrEjbFacade;
    }

    public void setUsrEjbFacade(UsuariosFacade usrEjbFacade) {
        this.usrEjbFacade = usrEjbFacade;
    }

    public void uploadAttachment(FileUploadEvent event) {

        System.out.println("Cargando... ");

        Usuarios usrActVist;
        //Optenemos el usuario actual vista
        
        FacesContext facesContext = FacesContext.getCurrentInstance();
        Map params = facesContext.getExternalContext().getRequestParameterMap();
        
        HttpSession sessionv = (HttpSession) facesContext.getExternalContext().getSession(true);
        usrActVist = (Usuarios) sessionv.getAttribute("usrActual");

        System.out.println("map.. " + (params.get("usrActual")));
        //usrActVist = (Usuarios) (params.get("usrActual"));
        System.out.println("usr actual: " + usrActVist);
        UploadedFile file = event.getFile();
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String webRoot = request.getRealPath("/");
        System.out.println("web root: " + webRoot);
        try {
            //Write Content
            //FileOutputStream out = new FileOutputStream("C://files//" + file.getFileName());
            //System.out.println(""+webRoot+"//Recursos//Imagenes//perfil//\" + file.getFileName()");
            FileOutputStream out = new FileOutputStream("" + webRoot + "Recursos/Imagenes/perfil/" + file.getFileName());
            out.write(file.getContents());
            out.close();
            System.out.println("Vamos actualizar la foto");
            usrActVist.setUsrFoto(file.getFileName());
            usrEjbFacade.edit(usrActVist);
            System.out.println("Archivo creado con Exito: ");
        } catch (FileNotFoundException e) {
            System.out.println("Error : " + e);
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error 2 : " + e);
        }
        System.out.println("finnnn");
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, String.format("Archivo cargado: %s ", file.getFileName()),
                String.format("Mensaje: %s", "")));

    }

}
