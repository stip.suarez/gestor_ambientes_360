package bean;


import clases.Usuarios;
import com.sun.xml.ws.developer.Serialization;
import controllers.UsuariosController;
import controllers.util.JsfUtil;
import controllers.util.PasswordService;
import java.io.IOException;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import facade.UsuariosFacade;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


@ManagedBean(name = "loggin")
//@Named("loggin")
@SessionScoped
@Serialization
public class Loggin implements InterfaceBean {
    @PersistenceContext(unitName = "gestor_ambientes_360PU")
    private EntityManager em;

   
    protected EntityManager getEntityManager() {
        return em;
    }
    private String cedula;
    private int ced;
    private String contrasena;
    private Usuarios usrActual;
    private String usrTipo;

   
    @EJB
    private UsuariosFacade usr;

    public Loggin(String cedula, String contrasena) {
        this.cedula = cedula;
        this.contrasena = contrasena;

    }

    public Usuarios getUsrActual() {
        return usrActual;
    }
    public void irPerfil() throws IOException{
        FacesContext.getCurrentInstance().getExternalContext().redirect("/gestor_ambientes_360/faces/perfiles.xhtml");
    }

    public void setUsrActual(Usuarios usrActual) {
        this.usrActual = usrActual;
    }
    
    public Loggin() {
    }

     public String getUsrTipo() {
        return usrTipo;
    }

    public void setUsrTipo(String usrTipo) {
        this.usrTipo = usrTipo;
    }
    public int getCed() {
        return ced;
    }

    public void setCed(int ced) {
        this.ced = ced;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public boolean isActiva() {
        return session.isActiva();

    }

    public boolean isAdmin(){
        try {
            int usr_rol=Integer.parseInt(""+usrActual.getUsrRolId().getRolId());
            boolean tipoUst = (usr_rol==1);
        return tipoUst==true;
        } catch (Exception e) {
             FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", ""+e+"\nLocalización: "+e.getLocalizedMessage()));
        }
        return false;
    }
      public Usuarios validarLogueo(String cedula, String contrasena) {
          try {
              
        //Consultar en la bd si està registrado
        int ced = Integer.parseInt(cedula);
        String sentencia = "SELECT * FROM Usuarios u WHERE u.usr_num_doc = '" + ced + "' AND u.usr_pass = '" + contrasena + "'";
        System.out.println(sentencia);
              try {
                  
        Query q = em.createNativeQuery(sentencia, Usuarios.class);
        System.out.println("2");
        Usuarios usr_encontrado =(Usuarios) q.getResultList().get(0);
        System.out.println("2.1");
              if (usr_encontrado!=null) {
                  System.out.println("Encontrado!!");
                  return (Usuarios) q.getResultList().get(0);
              }
       else return null;
              
              } catch (Exception e) {
                  JsfUtil.addErrorMessage("Verifique doc o pass");
                  System.out.println("Verifique doc o pass");
                  return null;
              }
        
          } catch (Exception e) {
              System.out.println("error al validar el logeo: "+e);
              return null;
          }
    }
    
    public void comprobar() {
        try {
            Usuarios user;
            //user = (Usuarios) usr.find(cedu);
            //Encriptamos la contraseña y la redefinimos
            contrasena=encriptarPass(contrasena);
            //System.out.println("Contraseña: "+contrasena);
            user=validarLogueo(cedula, contrasena);
            System.out.println("ver si es null: "+user);
            System.out.println("Estado: "+user.getUsrEstado());
            if (user != null) {
                if (user.getUsrEstado()) {
                     System.out.println("Iniciando Usuario Actual: "+user);
                UsuariosController.setCurrent(user);
                UsuariosController.setUsuarioActual(user);
                usrActual=user;
                setUsrActual(usrActual);
                //UsuariosController.setUsuarioActual(usrActual);
                FacesContext context2 = FacesContext.getCurrentInstance();
                HttpSession sessionv = (HttpSession) context2.getExternalContext().getSession(true);
                sessionv.setAttribute("user", usrActual);
                session.setActiva(true);
                JsfUtil.addSuccessMessage("Bienvenido "+usuarioActual().getUsrNombre());
                System.out.println("session iniciada con exito: "+usuarioActual().getUsrNombre());
                //FacesContext.getCurrentInstance().getExternalContext().redirect("/gestor_ambientes_360/");
                    System.out.println("session: "+session.isActiva());
                    System.out.println("usrAct: "+UsuariosController.getCurrent());
                irA("administracion");
                }else{
                 FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Su solicitud aùn no ha sido avalda... Espere" , ""));
                System.out.println("Su solicitud aun no ha sido procesada! Intente después");
                    irA("index");
                }
                                   
            } else {
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Verifique cédula y/o contraseña!", ""));
                System.out.println("Contraseña: "+contrasena);

            }
        } catch (Exception e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Verifique cédula y/o contraseña!  " + e, ""));
            System.out.println("Excepcion!!!....: "+e);
        }


    }
     public String encriptarPass(String palabra) { //Encripta la contraseñas
        //System.out.println("vamos a encriptar 000 Metodo encriptarPass ... palabra= "+palabra);
        PasswordService pws = PasswordService.getInstance();
        String hash;
        //System.out.println("vamos a encriptar... palabra a encriptar:"+palabra);
        try {
            hash = pws.encrypt(palabra);
            return hash;
        } catch (Exception e) {
            System.out.println("Error de encriptacion : "+e.toString());
            JsfUtil.addErrorMessage("Error de encriptación: "+e.toString());
            return null;
        }
    }
    public Usuarios usuarioActual(){
        return  UsuariosController.getCurrent();
    }

    public void cerrarSesion() throws IOException {

        UsuariosController.getCurrent().setUsrId(null);
        UsuariosController.setCurrent(new Usuarios());
        session.setActiva(false);
        JsfUtil.addSuccessMessage("Sesiòn cerrada con èxito!");
        System.out.println("Sesion cerrada con exito");
        FacesContext.getCurrentInstance().getExternalContext().redirect("/gestor_ambientes_360/");
    }
    
    public void irA(String dire) throws IOException{
        switch(dire){
            case "index": FacesContext.getCurrentInstance().getExternalContext().redirect("/gestor_ambientes_360/"); break;
            case "perfil": FacesContext.getCurrentInstance().getExternalContext().redirect("/gestor_ambientes_360/faces/web/usuarios/perfiles.xhtml"); break; 
            case "contacto": FacesContext.getCurrentInstance().getExternalContext().redirect("/gestor_ambientes_360/faces/componentes/contactenos.xhtml"); break;    
            case "administracion": FacesContext.getCurrentInstance().getExternalContext().redirect("/gestor_ambientes_360/faces/web/usuarios/administracion.xhtml"); break;    
            
        }
    }
    
    public void validarUsuario() throws IOException {

        if (usr.validarLogueo(cedula, contrasena)) {
            
        } else {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Verifique cédula y/o contraseña!", ""));

        }
    }

//Hay un problema con el redireccionamiento. Me tocó poner la dirección faces/crearUsuario.xhtml en lugar de red_dinamica/crearUsuario.xhtml para que
//pudiera funcionar.

    public void solicitarInscripcion() throws IOException {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Ingreso", "Bienvenido " + cedula));
        context.addMessage(null, new FacesMessage("Second Message", "Additional Info Here..."));
        FacesContext.getCurrentInstance().getExternalContext().redirect("/gestor_ambientes_360/faces/web/usuarios/registro_usr.xhtml");

    }
   
}
