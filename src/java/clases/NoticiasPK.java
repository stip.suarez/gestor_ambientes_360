/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import com.sun.istack.NotNull;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author HOME
 */
@Embeddable
public class NoticiasPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "noticia_id")
    private int noticiaId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "noticia_usr_id")
    private int noticiaUsrId;

    public NoticiasPK() {
    }

    public NoticiasPK(int noticiaId, int noticiaUsrId) {
        this.noticiaId = noticiaId;
        this.noticiaUsrId = noticiaUsrId;
    }

    public int getNoticiaId() {
        return noticiaId;
    }

    public void setNoticiaId(int noticiaId) {
        this.noticiaId = noticiaId;
    }

    public int getNoticiaUsrId() {
        return noticiaUsrId;
    }

    public void setNoticiaUsrId(int noticiaUsrId) {
        this.noticiaUsrId = noticiaUsrId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) noticiaId;
        hash += (int) noticiaUsrId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NoticiasPK)) {
            return false;
        }
        NoticiasPK other = (NoticiasPK) object;
        if (this.noticiaId != other.noticiaId) {
            return false;
        }
        if (this.noticiaUsrId != other.noticiaUsrId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "clases.NoticiasPK[ noticiaId=" + noticiaId + ", noticiaUsrId=" + noticiaUsrId + " ]";
    }
    
}
