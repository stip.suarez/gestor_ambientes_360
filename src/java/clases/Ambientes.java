/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author HOME
 */
@Entity
@Table(name = "ambientes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ambientes.findAll", query = "SELECT a FROM Ambientes a"),
    @NamedQuery(name = "Ambientes.findByAmbienteId", query = "SELECT a FROM Ambientes a WHERE a.ambienteId = :ambienteId"),
    @NamedQuery(name = "Ambientes.findByAmbienteNombre", query = "SELECT a FROM Ambientes a WHERE a.ambienteNombre = :ambienteNombre"),
    @NamedQuery(name = "Ambientes.findByAmbienteImg360", query = "SELECT a FROM Ambientes a WHERE a.ambienteImg360 = :ambienteImg360")})
public class Ambientes implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ambiente_id")
    private Integer ambienteId;
    @Column(name = "ambiente_nombre")
    private String ambienteNombre;
   
    @Column(name = "ambiente_img360")
    private String ambienteImg360;
    @ManyToMany(mappedBy = "ambientesCollection")
    private Collection<Acciones> accionesCollection;
    @JoinTable(name = "usuario_getina_ambiente", joinColumns = {
        @JoinColumn(name = "usr_gestiona_ambiente_id", referencedColumnName = "ambiente_id")}, inverseJoinColumns = {
        @JoinColumn(name = "usr_gestiona_usr_id", referencedColumnName = "usr_id")})
    @ManyToMany
    private Collection<Usuarios> usuariosCollection;
    @JoinColumn(name = "inventario_inventario_id", referencedColumnName = "inventario_id")
    @ManyToOne(optional = false)
    private Inventarios inventarioInventarioId;

    public Ambientes() {
    }

    public Ambientes(Integer ambienteId) {
        this.ambienteId = ambienteId;
    }

    public Integer getAmbienteId() {
        return ambienteId;
    }

    public void setAmbienteId(Integer ambienteId) {
        this.ambienteId = ambienteId;
    }

    public String getAmbienteNombre() {
        return ambienteNombre;
    }

    public void setAmbienteNombre(String ambienteNombre) {
        this.ambienteNombre = ambienteNombre;
    }

    public String getAmbienteImg360() {
        return ambienteImg360;
    }

    public void setAmbienteImg360(String ambienteImg360) {
        this.ambienteImg360 = ambienteImg360;
    }

    @XmlTransient
    public Collection<Acciones> getAccionesCollection() {
        return accionesCollection;
    }

    public void setAccionesCollection(Collection<Acciones> accionesCollection) {
        this.accionesCollection = accionesCollection;
    }

    @XmlTransient
    public Collection<Usuarios> getUsuariosCollection() {
        return usuariosCollection;
    }

    public void setUsuariosCollection(Collection<Usuarios> usuariosCollection) {
        this.usuariosCollection = usuariosCollection;
    }

    public Inventarios getInventarioInventarioId() {
        return inventarioInventarioId;
    }

    public void setInventarioInventarioId(Inventarios inventarioInventarioId) {
        this.inventarioInventarioId = inventarioInventarioId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ambienteId != null ? ambienteId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ambientes)) {
            return false;
        }
        Ambientes other = (Ambientes) object;
        if ((this.ambienteId == null && other.ambienteId != null) || (this.ambienteId != null && !this.ambienteId.equals(other.ambienteId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "clases.Ambientes[ ambienteId=" + ambienteId + " ]";
    }
    
}
