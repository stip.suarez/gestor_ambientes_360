/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author HOME
 */
@Entity
@Table(name = "inventarios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Inventarios.findAll", query = "SELECT i FROM Inventarios i")
    ,
    @NamedQuery(name = "Inventarios.findByInventarioId", query = "SELECT i FROM Inventarios i WHERE i.inventarioId = :inventarioId")
    ,
    @NamedQuery(name = "Inventarios.findByInventarioNombre", query = "SELECT i FROM Inventarios i WHERE i.inventarioNombre = :inventarioNombre")
    ,
    @NamedQuery(name = "Inventarios.findByInventarioCod", query = "SELECT i FROM Inventarios i WHERE i.inventarioCod = :inventarioCod")})
public class Inventarios implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "inventario_id")
    private Integer inventarioId;
    @Column(name = "inventario_nombre")
    private String inventarioNombre;
    @Column(name = "inventario_cod")
    private String inventarioCod;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "inventarioInventarioId")
    private Collection<Ambientes> ambientesCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "itemInventarioId")
    private Collection<Items> itemsCollection;

    public Inventarios() {
    }

    public Inventarios(Integer inventarioId) {
        this.inventarioId = inventarioId;
    }

    public Integer getInventarioId() {
        return inventarioId;
    }

    public void setInventarioId(Integer inventarioId) {
        this.inventarioId = inventarioId;
    }

    public String getInventarioNombre() {
        return inventarioNombre;
    }

    public void setInventarioNombre(String inventarioNombre) {
        this.inventarioNombre = inventarioNombre;
    }

    public void setInventarioNombreNuevo(String inventarioNombre) {
       // System.out.println("Nombre:  " + inventarioNombre);
        this.inventarioNombre = inventarioNombre;
    }

    public String getInventarioCod() {
        return inventarioCod;
    }

    public void setInventarioCod(String inventarioCod) {
        this.inventarioCod = inventarioCod;
    }

    public void setInventarioCodNuevo(String inventarioCod) {
        //System.out.println("Cod:  " + inventarioCod);
        this.inventarioCod = inventarioCod;
    }

    @XmlTransient
    public Collection<Ambientes> getAmbientesCollection() {
        return ambientesCollection;
    }

    public void setAmbientesCollection(Collection<Ambientes> ambientesCollection) {
        this.ambientesCollection = ambientesCollection;
    }

    @XmlTransient
    public Collection<Items> getItemsCollection() {

        return itemsCollection;
    }

    public void setItemsCollection(Collection<Items> itemsCollection) {
        this.itemsCollection = itemsCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (inventarioId != null ? inventarioId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Inventarios)) {
            return false;
        }
        Inventarios other = (Inventarios) object;
        if ((this.inventarioId == null && other.inventarioId != null) || (this.inventarioId != null && !this.inventarioId.equals(other.inventarioId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Inventarios{" + "inventarioId=" + inventarioId + ", inventarioNombre=" + inventarioNombre + ", inventarioCod=" + inventarioCod + ", ambientesCollection=" + ambientesCollection + ", itemsCollection=" + itemsCollection + '}';
    }

}
