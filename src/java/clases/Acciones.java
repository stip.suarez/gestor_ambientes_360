/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author HOME
 */
@Entity
@Table(name = "acciones")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Acciones.findAll", query = "SELECT a FROM Acciones a"),
    @NamedQuery(name = "Acciones.findByAccionesId", query = "SELECT a FROM Acciones a WHERE a.accionesId = :accionesId"),
    @NamedQuery(name = "Acciones.findByAccionesNombre", query = "SELECT a FROM Acciones a WHERE a.accionesNombre = :accionesNombre")})
public class Acciones implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "acciones_id")
    private Integer accionesId;
    @Column(name = "acciones_nombre")
    private String accionesNombre;
    @JoinTable(name = "ambientes_tiene_acciones", joinColumns = {
        @JoinColumn(name = "ambiente_tiene_acciones_id", referencedColumnName = "acciones_id")}, inverseJoinColumns = {
        @JoinColumn(name = "ambiente_tiene_ambiente_id", referencedColumnName = "ambiente_id")})
    @ManyToMany
    private Collection<Ambientes> ambientesCollection;
    @JoinTable(name = "rol_tiene_acciones", joinColumns = {
        @JoinColumn(name = "tiene_acci_idacciones", referencedColumnName = "acciones_id")}, inverseJoinColumns = {
        @JoinColumn(name = "tiene_acci_rol_id", referencedColumnName = "rol_id")})
    @ManyToMany
    private Collection<Rol> rolCollection;

    public Acciones() {
    }

    public Acciones(Integer accionesId) {
        this.accionesId = accionesId;
    }

    public Integer getAccionesId() {
        return accionesId;
    }

    public void setAccionesId(Integer accionesId) {
        this.accionesId = accionesId;
    }

    public String getAccionesNombre() {
        return accionesNombre;
    }

    public void setAccionesNombre(String accionesNombre) {
        this.accionesNombre = accionesNombre;
    }

    public void setAccionesNombreNuevo(String accionesNombre) {
        this.accionesNombre = accionesNombre;
    }

    @XmlTransient
    public Collection<Ambientes> getAmbientesCollection() {
        return ambientesCollection;
    }

    public void setAmbientesCollection(Collection<Ambientes> ambientesCollection) {
        this.ambientesCollection = ambientesCollection;
    }

    @XmlTransient
    public Collection<Rol> getRolCollection() {
        return rolCollection;
    }

    public void setRolCollection(Collection<Rol> rolCollection) {
        this.rolCollection = rolCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (accionesId != null ? accionesId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Acciones)) {
            return false;
        }
        Acciones other = (Acciones) object;
        if ((this.accionesId == null && other.accionesId != null) || (this.accionesId != null && !this.accionesId.equals(other.accionesId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "clases.Acciones[ accionesId=" + accionesId + " ]";
    }
    
}
