/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import com.sun.istack.NotNull;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author HOME
 */
@Entity
@Table(name = "items")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Items.findAll", query = "SELECT i FROM Items i"),
    @NamedQuery(name = "Items.findByItemId", query = "SELECT i FROM Items i WHERE i.itemId = :itemId"),
    @NamedQuery(name = "Items.findByItemNombre", query = "SELECT i FROM Items i WHERE i.itemNombre = :itemNombre"),
    @NamedQuery(name = "Items.findByItemCodigo", query = "SELECT i FROM Items i WHERE i.itemCodigo = :itemCodigo")})
public class Items implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "item_id")
    private Integer itemId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "item_nombre")
    private String itemNombre;
    @Column(name = "item_codigo")
    private String itemCodigo;
    @JoinColumn(name = "item_inventario_id", referencedColumnName = "inventario_id")
    @ManyToOne(optional = false)
    private Inventarios itemInventarioId;

    public Items() {
    }

    public Items(Integer itemId) {
        this.itemId = itemId;
    }

    public Items(Integer itemId, String itemNombre) {
        this.itemId = itemId;
        this.itemNombre = itemNombre;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public String getItemNombre() {
        return itemNombre;
    }

    public void setItemNombre(String itemNombre) {
        this.itemNombre = itemNombre;
    }
    public void setItemNombreNuevo(String itemNombre) {
        this.itemNombre = itemNombre;
    }

    public String getItemCodigo() {
        return itemCodigo;
    }

    public void setItemCodigo(String itemCodigo) {
        this.itemCodigo = itemCodigo;
    }
    
    public void setItemCodigoNuevo(String itemCodigo) {
        this.itemCodigo = itemCodigo;
    }

    public Inventarios getItemInventarioId() {
        return itemInventarioId;
    }

    public void setItemInventarioId(Inventarios itemInventarioId) {
        this.itemInventarioId = itemInventarioId;
    }
    
     public void setItemInventarioIdNuevo(Inventarios itemInventarioId) {
        this.itemInventarioId = itemInventarioId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (itemId != null ? itemId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Items)) {
            return false;
        }
        Items other = (Items) object;
        if ((this.itemId == null && other.itemId != null) || (this.itemId != null && !this.itemId.equals(other.itemId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Items{" + "itemId=" + itemId + ", itemNombre=" + itemNombre + ", itemCodigo=" + itemCodigo + ", itemInventarioId=" + itemInventarioId + '}';
    }

  
}
