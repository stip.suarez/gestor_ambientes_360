/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import com.sun.istack.NotNull;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author HOME
 */
@Entity
@Table(name = "usuarios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Usuarios.findAll", query = "SELECT u FROM Usuarios u"),
    @NamedQuery(name = "Usuarios.findByUsrId", query = "SELECT u FROM Usuarios u WHERE u.usrId = :usrId"),
    @NamedQuery(name = "Usuarios.findByUsrTipoDoc", query = "SELECT u FROM Usuarios u WHERE u.usrTipoDoc = :usrTipoDoc"),
    @NamedQuery(name = "Usuarios.findByUsrNumDoc", query = "SELECT u FROM Usuarios u WHERE u.usrNumDoc = :usrNumDoc"),
    @NamedQuery(name = "Usuarios.findByUsrNombre", query = "SELECT u FROM Usuarios u WHERE u.usrNombre = :usrNombre"),
    @NamedQuery(name = "Usuarios.findByUsrApellido", query = "SELECT u FROM Usuarios u WHERE u.usrApellido = :usrApellido"),
    @NamedQuery(name = "Usuarios.findByUsrPass", query = "SELECT u FROM Usuarios u WHERE u.usrPass = :usrPass"),
    @NamedQuery(name = "Usuarios.findByUsrEmail", query = "SELECT u FROM Usuarios u WHERE u.usrEmail = :usrEmail"),
    @NamedQuery(name = "Usuarios.findByUsrSexo", query = "SELECT u FROM Usuarios u WHERE u.usrSexo = :usrSexo"),
    @NamedQuery(name = "Usuarios.findByUsrEstado", query = "SELECT u FROM Usuarios u WHERE u.usrEstado = :usrEstado"),
    @NamedQuery(name = "Usuarios.findByUsrFoto", query = "SELECT u FROM Usuarios u WHERE u.usrFoto = :usrFoto")})
public class Usuarios implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "usr_id")
    private Integer usrId;
    @Column(name = "usr_tipo_doc")
    private String usrTipoDoc;
    @Column(name = "usr_num_doc")
    private String usrNumDoc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "usr_nombre")
    private String usrNombre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "usr_apellido")
    private String usrApellido;
    @Basic(optional = false)
    @NotNull
    @Column(name = "usr_pass")
    private String usrPass;
    
    @Column(name = "usr_email")
    private String usrEmail;
    @Column(name = "usr_sexo")
    private Boolean usrSexo;
    @Column(name = "usr_estado")
    private Boolean usrEstado;
    
    @Column(name = "usr_foto")
    private String usrFoto;
    @ManyToMany(mappedBy = "usuariosCollection")
    private Collection<Ambientes> ambientesCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "usuarios")
    private Collection<Noticias> noticiasCollection;
    @JoinColumn(name = "usr_ciudad_id", referencedColumnName = "ciudad_id")
    @ManyToOne(optional = false)
    private Ciudad usrCiudadId;
    @JoinColumn(name = "usr_departamento_id", referencedColumnName = "departamento_id")
    @ManyToOne(optional = false)
    private Departamentos usrDepartamentoId;
    @JoinColumn(name = "usr_rol_id", referencedColumnName = "rol_id")
    @ManyToOne(optional = false)
    private Rol usrRolId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "solicitudUsrId")
    private Collection<Solicitudes> solicitudesCollection;

    public Usuarios() {
    }

    public Usuarios(Integer usrId) {
        this.usrId = usrId;
    }

    public Usuarios(Integer usrId, String usrNombre, String usrApellido, String usrPass) {
        this.usrId = usrId;
        this.usrNombre = usrNombre;
        this.usrApellido = usrApellido;
        this.usrPass = usrPass;
    }

    public Integer getUsrId() {
        return usrId;
    }

    public void setUsrId(Integer usrId) {
        this.usrId = usrId;
    }

    public String getUsrTipoDoc() {
        return usrTipoDoc;
    }

    public void setUsrTipoDoc(String usrTipoDoc) {
        this.usrTipoDoc = usrTipoDoc;
    }

    public String getUsrNumDoc() {
        
        return usrNumDoc;
    }

    public void setUsrNumDoc(String usrNumDoc) {
        this.usrNumDoc = usrNumDoc;
    }

    public String getUsrNombre() {
        return usrNombre;
    }

    public void setUsrNombre(String usrNombre) {
        this.usrNombre = usrNombre;
    }

    public String getUsrApellido() {
        return usrApellido;
    }

    public void setUsrApellido(String usrApellido) {
        this.usrApellido = usrApellido;
    }

    public String getUsrPass() {
        return usrPass;
    }

    public void setUsrPass(String usrPass) {
        this.usrPass = usrPass;
    }

    public String getUsrEmail() {
        return usrEmail;
    }

    public void setUsrEmail(String usrEmail) {
        this.usrEmail = usrEmail;
    }

    public Boolean getUsrSexo() {
        return usrSexo;
    }

    public void setUsrSexo(Boolean usrSexo) {
        this.usrSexo = usrSexo;
    }

    public Boolean getUsrEstado() {
        return usrEstado;
    }

    public void setUsrEstado(Boolean usrEstado) {
        this.usrEstado = usrEstado;
    }

    public String getUsrFoto() {
        return usrFoto;
    }

    public void setUsrFoto(String usrFoto) {
        this.usrFoto = usrFoto;
    }

    @XmlTransient
    public Collection<Ambientes> getAmbientesCollection() {
        return ambientesCollection;
    }

    public void setAmbientesCollection(Collection<Ambientes> ambientesCollection) {
        this.ambientesCollection = ambientesCollection;
    }

    @XmlTransient
    public Collection<Noticias> getNoticiasCollection() {
        return noticiasCollection;
    }

    public void setNoticiasCollection(Collection<Noticias> noticiasCollection) {
        this.noticiasCollection = noticiasCollection;
    }

    public Ciudad getUsrCiudadId() {
        return usrCiudadId;
    }

    public void setUsrCiudadId(Ciudad usrCiudadId) {
        this.usrCiudadId = usrCiudadId;
    }

    public Departamentos getUsrDepartamentoId() {
        return usrDepartamentoId;
    }

    public void setUsrDepartamentoId(Departamentos usrDepartamentoId) {
        this.usrDepartamentoId = usrDepartamentoId;
    }

    public Rol getUsrRolId() {
        return usrRolId;
    }

    public void setUsrRolId(Rol usrRolId) {
        this.usrRolId = usrRolId;
    }

    @XmlTransient
    public Collection<Solicitudes> getSolicitudesCollection() {
        return solicitudesCollection;
    }

    public void setSolicitudesCollection(Collection<Solicitudes> solicitudesCollection) {
        this.solicitudesCollection = solicitudesCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usrId != null ? usrId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usuarios)) {
            return false;
        }
        Usuarios other = (Usuarios) object;
        if ((this.usrId == null && other.usrId != null) || (this.usrId != null && !this.usrId.equals(other.usrId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "clases.Usuarios[ usrId=" + usrId + " ]";
    }
    
}
