/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.faces.event.BehaviorEvent;

/**
 *
 * @author HOME
 */
@Entity
@Table(name = "noticias")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Noticias.findAll", query = "SELECT n FROM Noticias n"),
    @NamedQuery(name = "Noticias.findByNoticiaId", query = "SELECT n FROM Noticias n WHERE n.noticiasPK.noticiaId = :noticiaId"),
    @NamedQuery(name = "Noticias.findByNoticiaTitulo", query = "SELECT n FROM Noticias n WHERE n.noticiaTitulo = :noticiaTitulo"),
    @NamedQuery(name = "Noticias.findByNoticiaFecha", query = "SELECT n FROM Noticias n WHERE n.noticiaFecha = :noticiaFecha"),
    @NamedQuery(name = "Noticias.findByNoticiaImagen", query = "SELECT n FROM Noticias n WHERE n.noticiaImagen = :noticiaImagen"),
    @NamedQuery(name = "Noticias.findByNoticiaFuente", query = "SELECT n FROM Noticias n WHERE n.noticiaFuente = :noticiaFuente"),
    @NamedQuery(name = "Noticias.findByNoticiaUsrId", query = "SELECT n FROM Noticias n WHERE n.noticiasPK.noticiaUsrId = :noticiaUsrId")})
public class Noticias implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected NoticiasPK noticiasPK;
    @Column(name = "noticia_titulo")
    private String noticiaTitulo;
    @Column(name = "noticia_fecha")
    @Temporal(TemporalType.DATE)
    private Date noticiaFecha;
    @Column(name = "noticia_imagen")
    private String noticiaImagen;
    @Lob
    @Column(name = "noticia_descripcion")
    private String noticiaDescripcion;
    @Column(name = "noticia_fuente")
    private String noticiaFuente;
    @JoinColumn(name = "noticia_usr_id", referencedColumnName = "usr_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Usuarios usuarios;

    public Noticias() {
    }

    public Noticias(NoticiasPK noticiasPK) {
        this.noticiasPK = noticiasPK;
    }

    public Noticias(int noticiaId, int noticiaUsrId) {
        this.noticiasPK = new NoticiasPK(noticiaId, noticiaUsrId);
    }

    public NoticiasPK getNoticiasPK() {
        return noticiasPK;
    }

    public void setNoticiasPK(NoticiasPK noticiasPK) {
        this.noticiasPK = noticiasPK;
    }

    public String getNoticiaTitulo() {
        return noticiaTitulo;
    }

    public void setNoticiaTitulo(String noticiaTitulo) {
        this.noticiaTitulo = noticiaTitulo;
    }

    public Date getNoticiaFecha() {
        return noticiaFecha;
    }

    public void setNoticiaFecha(Date noticiaFecha) {
        this.noticiaFecha = noticiaFecha;
    }

        public void setNoticiaFechaNueva(Date noticiaFecha,BehaviorEvent event) {
        //System.out.println("Fecha nueva: "+event+"/n "+noticiaFecha);    
        this.noticiaFecha = noticiaFecha;
    }

    public String getNoticiaImagen() {
        return noticiaImagen;
    }

    public void setNoticiaImagen(String noticiaImagen) {
        this.noticiaImagen = noticiaImagen;
    }

    public String getNoticiaDescripcion() {
        return noticiaDescripcion;
    }

    public void setNoticiaDescripcion(String noticiaDescripcion) {
        //System.out.println("Descripcion:\n "+noticiaDescripcion);
        this.noticiaDescripcion = noticiaDescripcion;
    }

    public void setNoticiaDescripcionNueva(String noticiaDescripcion,BehaviorEvent event) {
       // System.out.println("Descrip nueva: "+event+"/n "+noticiaDescripcion);
        this.noticiaDescripcion = noticiaDescripcion;
    }
    public String getNoticiaFuente() {
        return noticiaFuente;
    }

    public void setNoticiaFuente(String noticiaFuente) {
        this.noticiaFuente = noticiaFuente;
    }

    public void setNoticiaFuenteNueva(String noticiaFuente, BehaviorEvent event) {
        //System.out.println("Evet: "+event+"/n "+noticiaFuente);
        this.noticiaFuente = noticiaFuente;
    }


    public Usuarios getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(Usuarios usuarios) {
        this.usuarios = usuarios;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (noticiasPK != null ? noticiasPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Noticias)) {
            return false;
        }
        Noticias other = (Noticias) object;
        if ((this.noticiasPK == null && other.noticiasPK != null) || (this.noticiasPK != null && !this.noticiasPK.equals(other.noticiasPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Noticias{" + "noticiasPK=" + noticiasPK + ", noticiaTitulo=" + noticiaTitulo + ", noticiaFecha=" + noticiaFecha + ", noticiaImagen=" + noticiaImagen + ", noticiaDescripcion=" + noticiaDescripcion + ", noticiaFuente=" + noticiaFuente + ", usuarios=" + usuarios + '}';
    }


    
}
